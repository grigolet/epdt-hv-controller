import pyhvwrapper
import zmq
import zmq.asyncio
import socket
import logging
import asyncio
import typing as t
import models as m
from correction import Correction


class HVControlBackgroundTask:
    def __init__(self, module: str, sys_type: int, username: str, password: str, zmq_address: str,
                 zmq_command_port: str, zmq_pub_port: str,
                 correction: Correction, correction_time: int, subscription_parameters=None, *args, **kwargs):
        """Main HV module control class.

        This class is responsible for controlling the HV module. It does it
        by running a loop that listens and publishes ZMQ messages. It also
        has a method for running an additional loop that corrects the related
        parameter according to the correction class passed as argument

        Parameters
        ----------
        module: str
            the name of the module (without host)
        sys_type: int
            the HVWrapper related sys_type
        username: str
            username to login to the module
        password: str
            password to login to the module
        zmq_address: str
            the protocol and interface over which to open zmq sockets
        zmq_command_port: int
            the port to be used for commands received by the main app
        zmq_sub_port: int
            the port used to regularly publish module's data
        correction: Correction
            the class used to correct data for pressure and temperature
        subscription_parameters: optional list
            the HVWrapper parameters that are regularly published. Default to
            ['V0Set', 'VMon', 'I0Set', 'IMon', 'Status']

        """
        self.module = module
        self.ip_address = self.get_ip_address(self.module)
        self.username = username
        self.password = password
        self.sys_type = sys_type
        self.zmq_address = zmq_address
        self.zmq_command_port = zmq_command_port
        self.zmq_pub_port = zmq_pub_port
        self.state = {}
        self.correction = correction
        self.subscription_parameters = subscription_parameters or [
            'V0Set', 'VMon', 'I0Set', 'IMon', 'Status', 'Pw']
        self.correction_parameters = ["V0Set"]  # hard coded for now
        self.correction_time = correction_time
        self.logger = logging.getLogger(__name__)
        self.poll_time = 0.01
        self.hv = pyhvwrapper.HV(
            self.ip_address, self.sys_type, self.username, self.password, logger=None)

    def get_ip_address(self, module: str):
        """Return the ip address of a dns name"""
        return socket.gethostbyname(module)

    def handle_crate_map_command(self):
        """Get the crate map associated to the module.

        Returns
        dict:
            a response in the form. The response is validated throught a pydantic
            model:
            {hvwrapper_code: int, message: str, data: dict}
        """
        if self.hv.last_result_code != 0:
            return_code = self.hv.connect()
            if return_code != 0:
                self.logger.debug(f"Connection returned {return_code}")
                return {"hvwrapper_code": return_code, "message": pyhvwrapper.utils.error_codes[return_code]}
        crate_map = self.hv.crate_map
        self.logger.debug(crate_map)
        response = m.Response(hvwrapper_code=self.hv.last_result_code,
                              message=pyhvwrapper.utils.error_codes.get(
                                  self.hv.last_result_code, ""),
                              data=crate_map)
        return response.dict()

    def handle_get_command(self, slot: int, channels: t.List[int], parameter: str, *args, **kwargs):
        """Get a parameter from the module.

        Parameters
        ----------
        slot: int
            the slot number
        channels: list of ints
            a list of channel from which to get the parameter
        parameter: str
            the HVWrapper parameter name
        Return
        ------
        dict
            a response in the form. The response is validated throught a pydantic
            model:
            {hvwrapper_code: int, message: str, data: dict}
        """
        if self.hv.last_result_code != 0:
            return_code = self.hv.connect()
            if return_code != 0:
                self.logger.debug(f"Connection returned {return_code}")
                return {"hvwrapper_code": return_code, "message": pyhvwrapper.utils.error_codes[return_code]}
        try:
            parameters = self.hv.get_param(slot, channels, parameter)
            response = m.Response(hvwrapper_code=self.hv.last_result_code, message=pyhvwrapper.utils.error_codes.get(
                self.hv.last_result_code, ""), data=parameters)
        except Exception as e:
            self.logger.exception(f"Error in retrieving parameters: {e}")
            response = m.Response(
                hvwrapper_code=self.hv.last_result_code, message=f"Error in retrieving parameter: {e}", data=None)
        return response.dict()

    def handle_set_command(self, slot: int, channels: t.List[int],
                           parameter: str, value: t.Union[int, float], *args, **kwargs):
        """Set parameter of a module.

        If the name of the parameter is present in correction_parameters then
        also the internal state is updated

        Parameters
        ----------
        slot: int
            the slot number 
        channels: list
            the list of channels to set the parameter to
        parameter: str
            the HVWrapper parameter name
        value: int or float
            the value to set

        Return
        ------
        dict
            a response in the form. The response is validated throught a pydantic
            model:
            {hvwrapper_code: int, message: str, data: dict}
        """
        if self.hv.last_result_code != 0:
            return_code = self.hv.connect()
            self.logger.debug(f"Connection returned {return_code}")
            return {"hvwrapper_code": return_code, "message": pyhvwrapper.utils.error_codes[return_code]}
        try:
            for channel in channels:
                self.logger.debug(f'channel {channel} - parameter = {parameter} - state = {self.state[slot][channel]}')
                if (parameter == 'V0Set') and self.state[slot][channel][parameter].get('correction'):
                    hv_to_apply = self.correction.correct(value)
                    return_code = self.hv.set_param(
                        slot, channel, parameter, hv_to_apply)
                    self.logger.debug("Setting corrected value to %d", hv_to_apply)
                else:
                    return_code = self.hv.set_param(
                        slot, channel, parameter, value)
                # Update state for each channel: if toggle_correction is enabled change
                # the 'correction' key
                self.logger.debug("Result from HV module after setting parameter = %d", return_code)
                if return_code == 0 and parameter in self.correction_parameters:
                    self.state[slot][channel][parameter]['value'] = value
                    self.logger.debug(
                        f"Updated state because SET command was issued for {slot}, channel {channel}, parameter {parameter} with value {value}")
                else:
                    return_code = self.hv.set_param(
                        slot, channels, parameter, value)

            response = m.Response(hvwrapper_code=return_code, message=pyhvwrapper.utils.error_codes.get(
                return_code, ""), data={"slot": slot, "channels": channels, "parameter": parameter, "value": value})
        except Exception as e:
            self.logger.exception(f"Error in retrieving parameters: {e}")
            response = m.Response(
                hvwrapper_code=return_code, message=f"Error in retrieving parameters: {e}", data=None)
        return response.dict()

    def handle_kill(self, slot: int, channels: t.List[int], *args, **kwargs):
        """Kill selected channels.

        The kill will switch of the selected channels, change the'correction' property to 
        False. It also set the value of V0Set and Pw to 0

        Parameters
        ----------
        slot: int
            the slot number 
        channels list:
            the list of channels to set the parameter to

        Return
        ------
        dict
            a response in the form. The response is validated throught a pydantic
            model:
            {hvwrapper_code: int, message: str, data: dict}
        """
        if self.hv.last_result_code != 0:
            return_code = self.hv.connect()
            if return_code != 0:
                self.logger.debug(f"Connection returned {return_code}")
                return {"hvwrapper_code": return_code, "message": pyhvwrapper.utils.error_codes[return_code]}
        try:
            params = {
                "Pw": 0,
                "V0Set": 0
            }
            for param, value_to_set in params.items():
                return_code = self.hv.set_param(
                    slot, channels, param, value_to_set)
                self.logger.debug(
                    f"Set {param} to {value_to_set}. Return code: {return_code}")
                if return_code != 0:
                    raise ValueError(
                        f"Could not set {param} to {value_to_set}: {pyhvwrapper.utils.error_codes[return_code]}")
                else:
                    # Update the state of each channel by disabling correction
                    # and setting parameter to 0
                    for channel in channels:
                        if param in self.correction_parameters:
                            self.state[slot][channel][param]['value'] = value_to_set
                            self.state[slot][channel][param]['correction'] = False
                        self.logger.debug(
                            f"Updated state because KILL command was issued for {slot}, channel {channel}")

            response = m.Response(
                hvwrapper_code=return_code, message=pyhvwrapper.utils.error_codes[return_code], data=None)
        except Exception as e:
            self.logger.exception(f"Error in retrieving parameters: {e}")
            response = m.Response(
                hvwrapper_code=-1, message=f"Error in KILL command: {e}", data=None)
        return response.dict()

    def toggle_correction(self, slot: int, channels: t.List[int], enable: bool,
                          correction_parameters: t.Optional[t.List[str]] = None,
                          *args, **kwargs):
        """Toggle correction by changing the state of the parameter with the
        desired value.

        The state of the object will be updated accordingly.


        Parameters
        ----------
        slot: int
            the slot number
        channels:
            the channels to use to toggle correction
        enable: bool
            wether to enable or disable correction
        correction_parameters: optional list of str
            a list of HVWrapper parameter that needs to be corrected. Default to 'V0Set'

        Return
        ------
        dict:
            a response in the form. The response is validated throught a pydantic
            model:
            {hvwrapper_code: int, message: str, data: dict or ""}

        """
        correction_parameters = correction_parameters or self.correction_parameters
        try:
            for channel in channels:
                for correction_parameter in correction_parameters:
                    self.state[slot][channel][correction_parameter]['correction'] = enable
            self.logger.debug(f"------UPDATED STATE: {self.module}-----------")
            self.logger.debug(self.state)
            response = m.Response(hvwrapper_code=0,
                                  message="State updated.", data=self.state)
        except Exception as e:
            self.logger.exception(f"Error in retrieving parameters: {e}")
            response = m.Response(
                hvwrapper_code=-1, message=f"Error in toggling correction: {e}", data=self.state)

        return response.dict()

    def handle_state(self):
        """Return a dict of the state of the object.

        Return
        ------
        dict:
            a response in the form. The response is validated throught a pydantic
            model:
            {hvwrapper_code: int, message: str, data: dict or ""}
        """
        response = m.Response(
            hvwrapper_code=0, message="State of the hv controller", data=self.state)
        return response.dict()

    def handle_command(self, msg: t.Dict[str, t.Any]):
        """Parse the zmq command received from the HTTP API. 

        This function parses the msg argument and dispatch the proper function

        Parameters
        ----------
        msg: dict
            a dict containing a 'command' key and an 'args' one.

        Return
        ------
        dict
            a dict of the response model

        """
        command = msg.get('command')
        args = msg.get('args')
        self.logger.debug(f"Parsing command from payload: {msg}")
        if command == 'CRATE_MAP':
            return self.handle_crate_map_command()
        elif command == 'GET':
            return self.handle_get_command(**args)
        elif command == 'SET':
            return self.handle_set_command(**args)
        elif command == 'TOGGLE_CORRECTION':
            return self.toggle_correction(**args)
        elif command == 'KILL':
            return self.handle_kill(**args)
        elif command == 'STATE':
            return self.handle_state()
        else:
            response = m.Response(
                hvwrapper_code=-1, message=f"Command {command} not understood.", data=msg)
            return response.dict()

    def build_state(self):
        """Create a state of the hv module. 

        The state is a dict representing the status of each single channel in 
        terms of correction and kill. If a correction or kill command is sent
        the state is updated to reflect the command

        The state has the following structure
        state = {
            0: {  <- The slot number
                0: { <- The channel number
                    "V0Set": { <- The correction parameter
                        "value": 9600, <- The value to be used for correction. Initially it will be read from the module
                        "correction": True <- If correction should be enabled or not
                    }, 
                }
            }
        }

        """
        if self.hv.last_result_code != 0:
            self.logger.debug("Trying to connect")
            return_code = self.hv.connect()
            self.logger.debug(f"Attempted reconnection. Result: {return_code}")
            if return_code != 0:
                raise ValueError(
                    f"Could not create state because connection to the module returned code {return_code}")
        state = {}
        crate_map = self.hv.crate_map
        for slot_number, channel_objs in crate_map.items():
            state[slot_number] = {}
            num_channels = channel_objs['channels']
            channels = list(range(num_channels))
            for channel_number in channels:
                state[slot_number][channel_number] = {}
                for correction_parameter in self.correction_parameters:
                    self.logger.debug("Trying to get parameter on slot %d, channel %d, parameter %s",
                                      slot_number, channel_number, correction_parameter)
                    state[slot_number][channel_number][correction_parameter] = {
                        # Use the initial parameter value from the module as the value for the state
                        'value': self.hv.get_param(slot_number, channel_number, correction_parameter)[channel_number],
                        'correction': False,
                    }
        return state

    async def run_correction(self):
        """Main pressure/temperature correction loop.

        This loop runs infinitely and checks the state of the object. 
        For each channel and parameter with correction=True it will apply the
        correct() method from the self.correction class using the 'value' property of 
        the channel state. The resulted corrected
        value is set to the module and the state is updated accordingly.
        """
        # Iterate over each slot, over each channel and over each parameter
        # that should be corrected.
        while True:
            try:
                for slot_number, slot_obj in self.state.items():
                    for channel_number, channel_obj in slot_obj.items():
                        for correction_parameter_name, correction_config in channel_obj.items():
                            if correction_config['correction']:
                                # Get the desired efective hv
                                hv_to_apply = correction_config['value']
                                if self.hv.last_result_code != 0:
                                    return_code = self.hv.connect()
                                    self.logger.debug(
                                        f"Attempted reconnection with result: {return_code}")
                                    if return_code != 0:
                                        raise ValueError(
                                            f"Could not connect to hv module. Result code: {return_code} {pyhvwrapper.utils.error_codes[return_code]}")
                                try:
                                    hv_to_apply = self.correction.correct(
                                        hv_to_apply)
                                    self.logger.debug(
                                        f"Correction slot={slot_number} param={correction_parameter_name} hvapp={hv_to_apply:.0f} t={self.correction.t:.2f} p={self.correction.p:.1f} alpha={self.correction.alpha} beta={self.correction.beta}")
                                except ValueError as e:
                                    self.logger.warning(
                                        f"p/T Correction data not available: {e}")
                                self.hv.set_param(
                                    slot_number, channel_number, correction_parameter_name, hv_to_apply)
            except Exception as e:
                self.logger.exception(e)

            await asyncio.sleep(self.correction_time)

    async def get_module_data(self):
        """Return the state of the module (not of the HVCorrection object).
        The state of the module is the representation of the values of the 
        parameters of the single channels. The state is a dict with channel,
        parameter, slot, module and value.

        Return
        ------
        dict:
            the state of the module. Contains the following keys:
            channel: int
            parameter: str
            value: int or float
            slot: int
            module: str
        """
        data = []
        for parameter in self.subscription_parameters:
            for slot, board_map in self.hv.crate_map.items():
                channels = list(board_map['channels_map'].keys())
                res = self.hv.get_param(slot, channels, parameter)
                if self.hv.last_result_code != 0:
                    self.logger.debug(
                        f"Last result code is {self.hv.last_result_code}")
                    self.hv.connect()
                    self.logger.debug(
                        f"Last result code after reconnection is {self.hv.last_result_code}")
                for channel in channels:
                    state = self.state[slot][channel].get(parameter, {})
                    data.append({
                        'module': self.module,
                        'slot': slot,
                        'channel': channel,
                        'channel_name': board_map["channels_map"][channel],
                        'parameter': parameter,
                        'value': res[channel],
                        'state_value': state.get('value'),
                        'correction': state.get('correction')
                    })

        return data

    async def run(self):
        """Main loop listening for commands and publishing module state.

        This function listen to ZMQ commands coming from the fastapi and
        at the same time publishes via a ZMQ PUB socket the module state.
        """
        self.logger.debug(f"Socket created")
        ctx = zmq.asyncio.Context()
        self.zmq_command_socket = ctx.socket(zmq.PAIR)
        self.zmq_command_socket.connect(
            f"{self.zmq_address}:{self.zmq_command_port}")
        self.zmq_pub_socket = ctx.socket(zmq.PUB)
        self.zmq_pub_socket.setsockopt(zmq.SNDHWM, 2)
        self.zmq_pub_socket.setsockopt(zmq.SNDBUF, 2*1024)
        res = self.zmq_pub_socket.connect(
            f"{self.zmq_address}:{self.zmq_pub_port}")

        self.poller = zmq.asyncio.Poller()
        self.poller.register(self.zmq_command_socket, zmq.POLLIN)
        print("test")
        self.logger.debug("Initialized zmq socket and poller")
        self.state = self.build_state()
        self.logger.debug(f"Built initial state: {self.state}")
        print("test")
        # self.hv.connect()

        while True:
            if self.zmq_command_socket in dict(await self.poller.poll(self.poll_time * 1000)):
                msg = await self.zmq_command_socket.recv_json()
                self.logger.debug(f"Received json message {msg}")
                reply = self.handle_command(msg)
                self.logger.debug(f"Sending json reply {reply}")
                await self.zmq_command_socket.send_json(reply)

            self.data = await self.get_module_data()
            if self.hv.last_result_code != 0:
                # Attempt on connecting to the HV module
                res = self.hv.connect()
                self.logger.debug(f'Attempting connection on empty data {res}')
            else:
                self.zmq_pub_socket.send_string(
                    self.module, flags=zmq.SNDMORE)
                self.zmq_pub_socket.send_json(self.data)

            await asyncio.sleep(1)
