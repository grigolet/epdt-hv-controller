from fastapi import FastAPI, Response, status, Query, BackgroundTasks, WebSocket, Body
from fastapi.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware
import pyhvwrapper
import uvicorn
from box import Box
import os
from typing import Optional, Union, Dict
import zmq
import zmq.asyncio
import logging
import controller
import correction
import models
import asyncio
import utils as ut
import threading

tags_metadata = [{
    "name": "home",
    "description": "Home page redirected by default to /docs"
}, {
    "name": "get",
    "description": "Get a parameter from an HV module"
}, {
    "name": "set",
    "description": "Set a parameter on an HV module"
}, {
    "name": "crate_map",
    "description": "Get a crate map of an HV module"
}, {
    "name":
    "toggle",
    "description":
    "Toggle pressure/temperature correction on a channel of an HV module"
}, {
    "name":
    "kill",
    "description":
    "Kill channels and p/T correction of an HV module. The channels will stay off until a reset "
}]

origins = [
    "*"
]

logger = logging.getLogger('controller')
logging.getLogger("pyhvwrapper.HVWrapper").setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)
asyncio_logger = logging.getLogger('asyncio')
asyncio_logger.setLevel(logging.ERROR)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s - %(name)s - %(funcName)s - %(lineno)d - %(message)s")
app = FastAPI(description="""
                Power supply Controller module with HTTP REST api. 
                Use the API endpoints documented here below to understand how
                to control a power supply module via HTTP calls
                """,
              title='HV control module',
              openapi_tags=tags_metadata)
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True
)
# Dict to store HVControlBackground tasks
# so that they can be cleaned up on app shutdown
tasks = {}
CONFIG_PATH = os.environ.get('CONFIG_PATH', 'config.yaml')
SERVER_PORT = int(os.environ.get('SERVER_PORT', '5555'))
# Validate the config from the pydantic setting defined in models.py
config_box = Box.from_yaml(filename=CONFIG_PATH)
config = models.Config(**config_box.to_dict())
config = config_box


@app.on_event('startup')
async def startup():
    """Parse config, initialize tasks and socket connections.

    1. Parse config
    2. Create a proxy/broker to forard publications from hv_tasks to a single
    XPUB port
    3. Create an HVControlBackgroundTask object for each module
    4. Run main control loop (publications and commands)
    5. Run p/T correction loop
    """
    ctx = zmq.asyncio.Context()

    # Create the broker for message forwarding
    xpub_port, xsub_port = config.zmq.xpub_port, config.zmq.xsub_port
    asyncio.create_task(ut.broker(xpub_port, xsub_port))
    # For each module defined in config create a zmq tcp socket
    # to pass messages (both API <-> module)
    for hv_module in config.hv.modules:
        module = hv_module.name
        username = hv_module.username
        password = hv_module.password
        sys_type = hv_module.sys_type
        correction_time = hv_module.correction_time

        # Use a pair socket for commands
        socket = ctx.socket(zmq.PAIR)
        command_port = socket.bind_to_random_port("tcp://*")
        # Create the configuration object in the format required
        # by the correction class
        correction_config = Box()
        correction_config.db = config.db
        correction_config.module = hv_module
        logger.info(f"Correction config: {correction_config}")
        correction_obj = correction.Correction(correction_config)
        # Create an asyncio task. Run two functions concurrently
        hv_task = controller.HVControlBackgroundTask(
            module=module,
            sys_type=sys_type,
            username=username,
            password=password,
            correction=correction_obj,
            zmq_address=config.zmq.base_address,
            zmq_command_port=command_port,
            zmq_pub_port=xsub_port,
            correction_time=correction_time,
            subscription_parameters=hv_module.subscription_parameters)

        # 1. Task to exchange commands
        task_zmq = asyncio.create_task(hv_task.run())

        # 2. Task to control and adjust voltgae based on p/t correction
        task_correction = asyncio.create_task(hv_task.run_correction())

        # Save a map of the tasks ran
        tasks[module] = {
            "socket": socket,
            "task_zmq": task_zmq,
            "task_correction": task_correction,
            "command_port": command_port,
        }


@app.on_event("shutdown")
async def shutdown():
    """Function to be executed on shutdown.

    This function closes zmq socket and tries to stop
    asyncio tasks
    """
    print("Shutting down processes")
    for module, v in tasks.items():
        task_zmq = v.get('task_zmq')
        task_correction = v.get('task_correction')
        socket = v.get('socket')
        try:
            socket.close()
            task_zmq.cancel()
            task_correction.cancel()
            await task_zmq
            await task_correction
        except asyncio.CancelledError as e:
            print(f"Tasks for module {module} cancelled")
    ctx = zmq.asyncio.Context()
    ctx.term()


@app.get('/', tags=["home"])
async def home():
    """Redirect main page to /docs"""
    return RedirectResponse("/docs")


@app.get('/crate_map', tags=["crate_map"])
async def crate_map(response: Response,
                    module: str = Query(...,
                                        title="HV module",
                                        description="Name of the HV module")):
    """Return the crate map of an HV module. The HV module must be present
    in the config, otherwise a 400 error is returned. If the credentials are
    wrong or the module is offline or something else happen a 400 return code
    is sent with the 'message' property explaining the reason.

    If the command is correctly executed the returned response has a 'crate_map'
    property with the slot number as a key and a dict describing the number of
    channels, and their name mapping.
    """

    reply, response = await ut.send_command("CRATE_MAP", response, tasks,
                                            module)
    return reply


@app.get('/get', tags=["get"])
async def get_param(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        ...,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    parameter: str = Query(
        ...,
        title="Parameter",
        description=
        "The parameter from which to retrieve data. The name should be a valid CAEN HVWrapper parameter name."
    )):
    channels, response = ut.handle_channels(channels, response)

    reply, response = await ut.send_command("GET",
                                            response,
                                            tasks,
                                            module,
                                            username=username,
                                            password=password,
                                            slot=slot,
                                            channels=channels,
                                            parameter=parameter)
    return reply


@app.post('/set', tags=["set"])
async def post_set(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        ...,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    parameter: str = Query(
        ...,
        title="Parameter",
        description=
        "The parameter from which to retrieve data. The name should be a valid CAEN HVWrapper parameter name."
    ),
    value: Union[int, float] = Query(
        ...,
        title="Value",
        description=
        "The value of the parameter to set. If boolean like 'Pw' pass 1 for true and 0 for false"
    ),
    payload: Optional[dict] = Body(
        None,
        title="Optional json payload",
        description=
        "Mainly used for grafana. If a key 'state' is present it will check if the value is set to 'alerting'. Otherwise no action will be taken"
    )):
    grafana_discard_condition = payload and payload.get('state') != "alerting"

    if grafana_discard_condition:
        return models.Response(hvwrapper_code=None,
                               message="Payload state invalid",
                               data=payload)

    channels, response = ut.handle_channels(channels, response)

    reply, response = await ut.send_command("SET",
                                            response,
                                            tasks,
                                            module,
                                            username=username,
                                            password=password,
                                            slot=slot,
                                            channels=channels,
                                            parameter=parameter,
                                            value=value)
    return reply


@app.get('/set', tags=["set"])
async def get_set(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        ...,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    parameter: str = Query(
        ...,
        title="Parameter",
        description=
        "The parameter from which to retrieve data. The name should be a valid CAEN HVWrapper parameter name."
    ),
    value: Union[int, float] = Query(
        ...,
        title="Value",
        description=
        "The value of the parameter to set. If boolean like 'Pw' pass 1 for true and 0 for false"
    )):
    channels, response = ut.handle_channels(channels, response)

    reply, response = await ut.send_command("SET",
                                            response,
                                            tasks,
                                            module,
                                            username=username,
                                            password=password,
                                            slot=slot,
                                            channels=channels,
                                            parameter=parameter,
                                            value=value)
    return reply


@app.get('/toggle_correction', tags=["toggle"])
async def toggle_correction(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        ...,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    enable: bool = Query(
        ...,
        title="Enable",
        description=
        "Enable or disable the pressure/temperature correction. Use true/false, 1/0, on/off"
    )):
    channels, response = ut.handle_channels(channels, response)

    reply, response = await ut.send_command("TOGGLE_CORRECTION",
                                            response,
                                            tasks,
                                            module,
                                            username=username,
                                            password=password,
                                            slot=slot,
                                            channels=channels,
                                            enable=enable)
    return reply


@app.post("/start", tags=["start"])
async def handle_start(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        ...,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    value: Union[int, float] = Query(
        ..., title="Value", description="The value of the voltage to set."),
    enable: bool = Query(
        ...,
        title="Enable",
        description=
        "Enable or disable the pressure/temperature correction. Use true/false, 1/0, on/off"
    ),
    payload: Optional[dict] = Body(
        None,
        title="Optional json payload",
        description=
        "Mainly used for grafana. If a key 'state' is present it will check if the value is set to 'alerting'. Otherwise no action will be taken"
    )):
    grafana_discard_condition = payload and payload.get('state') != "alerting"

    if grafana_discard_condition:
        return models.Response(hvwrapper_code=None,
                               message="Payload state invalid",
                               data=payload)

    channels, response = ut.handle_channels(channels, response)
    
    reply_toggle, response = await ut.send_command("TOGGLE_CORRECTION",
                                                    response,
                                                    tasks,
                                                    module,
                                                    username=username,
                                                    password=password,
                                                    slot=slot,
                                                    channels=channels,
                                                    enable=enable)
    reply_set, response = await ut.send_command("SET",
                                                response,
                                                tasks,
                                                module,
                                                username=username,
                                                password=password,
                                                slot=slot,
                                                channels=channels,
                                                parameter="V0Set",
                                                value=value)
    reply_set, response = await ut.send_command("SET",
                                                response,
                                                tasks,
                                                module,
                                                username=username,
                                                password=password,
                                                slot=slot,
                                                channels=channels,
                                                parameter="Pw",
                                                value=True)
    logger.debug(f"Reply SET: {reply_set} response: {response.status_code}")
    

    return models.Response(hvwrapper_code=None,
                           message="Start command issued",
                           data={
                               "module": module,
                               "slot": slot,
                               "channels": channels,
                               "value": value,
                               "enable": enable,
                               "response_set": reply_set,
                               "response_toggle": reply_toggle
                           })


@app.post('/kill', tags=["kill"])
async def get_kill(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        None,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    payload: Optional[dict] = Body(
        None,
        title="Optional json payload",
        description=
        "Mainly used for grafana. If a key 'state' is present it will check if the value is set to 'alerting'. Otherwise no action will be taken"
    )):
    grafana_discard_condition = payload and payload.get('state') != "alerting"

    if grafana_discard_condition:
        return models.Response(hvwrapper_code=None,
                               message="Invalid state",
                               data=payload)

    return await ut.kill_handler(response, module, username, password, slot,
                                 channels, tasks)


@app.get('/kill', tags=["kill"])
async def post_kill(
    response: Response,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module"),
    username: str = Query(...,
                          title="Username",
                          description="Username to connect to the module"),
    password: str = Query(...,
                          title="Password",
                          description="Password to connect to the module"),
    slot: int = Query(..., title="Slot", description="The slot to be used"),
    channels: Union[str, int] = Query(
        None,
        title="Channels",
        description=
        "The channels from which to retrieve the parameter. It can be passed as asingle number (e.g. '2') or as a comma separated list (e.g. '2,4,6')"
    ),
    payload: Optional[dict] = Body(
        None,
        title="Optional json payload",
        description=
        "Mainly used for grafana. If a key 'state' is present it will check if the value is set to 'alerting'. Otherwise no action will be taken"
    )):
    grafana_discard_condition = payload and payload.get('state') != "alerting"

    if grafana_discard_condition:
        return models.Response(hvwrapper_code=None,
                               message="Invalid state",
                               data=payload)

    return await ut.kill_handler(response, module, username, password, slot,
                                 channels, tasks)


@app.websocket("/subscribe")
async def subscribe(
    websocket: WebSocket,
    module: str = Query(...,
                        title="HV module",
                        description="Name of the HV module")):
    await websocket.accept()
    ctx = zmq.asyncio.Context()
    logger.debug(f'Accepeted connection with module {module}')
    try:
        module = ut.format_module_name(module)
        socket = ctx.socket(zmq.SUB)
        socket.connect(f"{config.zmq.base_address}:{config.zmq.xpub_port}")
        socket.setsockopt_string(zmq.SUBSCRIBE, module)
        socket.setsockopt(zmq.SNDHWM, 2)
        socket.setsockopt(zmq.SNDBUF, 2 * 1024)
        logger.debug(f"Create sub socket and subscribed to {module}")
    except Exception as e:
        logger.exception(e)
        await websocket.close(1001)
        return

    while True:
        if not socket:
            await websocket.close(1002)
        try:
            topic = await socket.recv_string()
            payload = await socket.recv_json()
            logger.debug(f"Received zmq message within websocket loop")
            await websocket.send_json(payload)
            logger.debug(f"Sent zmq message within websocket loop")
        except Exception as e:
            logger.exception(e)
            return


if __name__ == '__main__':
    uvicorn.run("main:app", host='0.0.0.0', port=SERVER_PORT)
