from influxdb import InfluxDBClient
import urllib3
urllib3.disable_warnings()


class Correction:
    def __init__(self, config):
        self.db_config = config.db
        self.module_config = config.module
        self.influxdb_client = self.get_influxdb_client(self.db_config)
        self.p = None
        self.t = None
        self.p0 = self.module_config.pressure.p0
        self.t0 = self.module_config.temperature.t0 + 273.15
        self.beta = self.module_config.temperature.get('beta', 1)
        self.alpha = self.module_config.pressure.get('alpha', 1)

    def get_influxdb_client(self, db_config):
        client = InfluxDBClient(host=db_config.host, port=db_config.port, username=db_config.username,
                                password=db_config.password, database=db_config.database, ssl=db_config.ssl, verify_ssl=db_config.verify_ssl)
        return client

    def run_query(self, measurement, tags, field):
        tags_string = ''.join(
            [f""" "{k}" = '{v}' AND """ for k, v in tags.items()])
        query = f"""
            SELECT last("{field}") as value
            FROM "{measurement}" 
            WHERE {tags_string} 
            time > now() - 1h 
        """.strip()
        return self.influxdb_client.query(query)

    def parse_time_value(self, query_result):
        points = list(query_result.get_points())
        if not len(points):
            raise ValueError("No points are available from the query")
        last_point = points[-1]
        time, value = last_point['time'], last_point['value']
        return time, value

    def get_db_value(self, measurement, tags, field):
        res = self.run_query(measurement, tags, field)
        time, value = self.parse_time_value(res)
        return value

    def get_environmental_value(self, name):
        env_config = self.module_config[name]
        env_value = self.get_db_value(
            env_config.measurement, env_config.tags, env_config.field)
        return env_value

    def get_pressure(self):
        return self.get_environmental_value(name='pressure')

    def get_temperature(self):
        return self.get_environmental_value(name='temperature')

    def correct(self, hv_value):
        """Correct the voltage value"""
        self.p = self.get_pressure()
        self.t = self.get_temperature() + 273.15
        hv_to_apply = hv_value * (1 - self.beta * (self.t - self.t0) / self.t0) * (1 + self.alpha * (self.p - self.p0) / self.p0)
        return hv_to_apply
