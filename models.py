from pydantic import BaseModel
from pydantic_settings import BaseSettings
import typing as t


class Channel(BaseModel):
    number: int
    name: str
    V0Set: int
    I0Set: float
    VMon: int
    IMon: float
    Pw: bool
    Status: str
    Status_code: int
    RUp: int
    RDwn: int
    correction: bool = False


class Slot(BaseModel):
    number: int
    channels: t.List[Channel]


class Module(BaseModel):
    name: str
    slots: Slot


class PressureConfig(BaseModel):
    p0: float = 965
    measurement: str
    tags: t.Dict[str, str]
    field: str


class TemperatureConfig(BaseModel):
    t0: float = 20
    measurement: str
    tags: t.Dict[str, str]
    field: str


class ModuleConfig(BaseModel):
    name: str
    username: str
    password: str
    sys_type: int = 3
    subscription_parameters: t.List[str] = ["V0Set", "VMon", "I0Set", "IMon", "Pw", "Status"]
    pressure: PressureConfig
    correction_time: int = 60  # in seconds


class HVConfig(BaseModel):
    modules: t.List[ModuleConfig]


class DBConfig(BaseModel):
    host: str
    port: int
    username: str
    password: str
    database: str
    ssl: bool = True
    verify_ssl: bool = False


class ZMQConfig(BaseModel):
    xsub_port: int
    xpub_port: int
    base_address: str


class Config(BaseSettings):
    hv: HVConfig
    db: DBConfig
    zmq: ZMQConfig


class Response(BaseModel):
    hvwrapper_code: t.Optional[int] = None
    message: str
    data: t.Optional[t.Dict] = None
