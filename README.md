# HV Controller

Control High Voltage (HV) on RPC detectors for EP-DT2 GIF++ setup.

The project consists of a REST api to control the power supply module connected
to the RPCs detector used by EP-DT-FS Gas System group.

The REST api is provided so that the HV can be controlled by different sources
(web app, grafana, bot, etc.)

# Installation

With a working python 3 installation and a `conda` or `venv` environment you can
install the dependencies and run the main app.

```bash
$ git clone <url-to-this-repo>
$ pip install -r requirements.txt
$ python app.py
```

# Configuration

Configuration is managed via a `config.yaml` file. The path to the file can
be set via the environment variable `CONFIG_PATH`. An example of config file
looks like this

```yaml
hv:
  modules:
    - name: "caenmodulename.cern.ch"
      username: ""
      password: ""
      sys_type: 3
      pressure:
        p0: 965
        measurement: yoctopuce
        tags:
          place: cern
          setup: rpc
        field: pressure
      temperature:
        t0: 20
        measurement: yoctopuce
        tags:
          place: cern
          setup: rpc
        field: temperature

    - name: "caenmodulename.cern.ch"
      username: ""
      password: ""
      sys_type: 3
      pressure:
        p0: 965
        measurement: dip
        tags:
          sub: "dip/GIFpp/Atmospheric_Pressure"
        field: "Float_value"
      temperature:
        t0: 20
        measurement: dip
        tags:
          sub: "dip/GIFpp/Temp_Inside_Bunker"
        field: "Float_value"

zmq:
  xsub_port: 9799
  xpub_port: 9800
  base_address: "tcp://0.0.0.0"

db:
  host: "localhost"
  port: 8080
  username: ""
  password: ""
  database: db
  ssl: true
  verify_ssl: false
```

# Documentation

The app exposes a web server with HTTP REST APIs to get and set parameters
of a CAEN HV module. The set of parameters that can be controlled is limited
by the most useful ones for our needs.
The app is also running two background loops: the first is used to communicate
between the HTTP server and the control routine for the HV. The second is used
instead to set the voltage value with the proper pressure/temperature correction.

The communication between the HTTP server and the background loops is happening
via ZMQ messages over a PAIR sockets.

The app exposes also a PUB/SUB zmq service to subscribe to data read from the HV
module.

The HTTP APIs are documented on the endpoint `/docs`. In particular, the main
APIs are:

- `/crate_map` - `GET`: Returns the create map of a module
- `/get` - `GET`: Returns a parameter of a slot of one or more channels
- `/set` - `GET`, `POST`: Set a parameter of a slot of one or more channels
- `/toggle_correction` - `GET`, `POST`: Enable or disable p/T correction of one
  or more channels of a slot
- `/kill` - `GET`, `POST`: Put che hannels to power off and V0Set to 0
- `/start` - `POST`: Apply `/set` and `/toggle_correction` together. It starts
  a channel with eventual p/T correction

# Examples

Using [httpie](https://httpie.io/docs#querystring-parameters):

## Crate map

You only need to specify the `module` name as a query string:

```bash
$ http http://127.0.0.1:8000/crate_map module==hvrpc256new
HTTP/1.1 200 OK
content-length: 302
content-type: application/json
date: Sun, 09 May 2021 17:20:11 GMT
server: uvicorn

{
    "data": {
        "2": {
            "board": "A1526",
            "channels": 6,
            "channels_map": {
                "0": "RPC0",
                "1": "CH1",
                "2": "RPC2",
                "3": "RPC1",
                "4": "CH04",
                "5": "CH05"
            },
            "desc": " 6 Ch Neg. 15KV 1mA  ",
            "inverse_channels_map": {
                "CH04": 4,
                "CH05": 5,
                "CH1": 1,
                "RPC0": 0,
                "RPC1": 3,
                "RPC2": 2
            }
        }
    },
    "hvwrapper_code": 0,
    "message": "Command wrapper correctly executed"
}
```

## Get parameter

To get a parameter from a module:

```bash
$ http http://127.0.0.1:8000/get module==hvrpc256new username==username password==password parameter==I0Set slot==2 channels==2
HTTP/1.1 200 OK
content-length: 86
content-type: application/json
date: Sun, 09 May 2021 17:29:43 GMT
server: uvicorn

{
    "data": {
        "2": 250.0
    },
    "hvwrapper_code": 0,
    "message": "Command wrapper correctly executed"
}
```

You can specify multiple channels by separating them with a comma:

```bash
$ http http://127.0.0.1:8000/get module==hvrpc256new username==username password==password parameter==I0Set slot==2 channels==0,1,2
HTTP/1.1 200 OK
content-length: 106
content-type: application/json
date: Sun, 09 May 2021 17:30:14 GMT
server: uvicorn

{
    "data": {
        "0": 100.0,
        "1": 300.0,
        "2": 250.0
    },
    "hvwrapper_code": 0,
    "message": "Command wrapper correctly executed"
}
```

## Set a parameter

Similar to get a parameter, but you would have to specify also the value of
the parameter to set:

```bash
$ http http://127.0.0.1:8000/set module==hvrpc256new username==username password==password parameter==I0Set value==0 slot==2 channels==1
HTTP/1.1 200 OK
content-length: 130
content-type: application/json
date: Mon, 10 May 2021 07:10:19 GMT
server: uvicorn

{
    "data": {
        "channels": [
            1
        ],
        "parameter": "I0Set",
        "slot": 2,
        "value": 0
    },
    "hvwrapper_code": 0,
    "message": "Command wrapper correctly executed"
}
```

```bash
$ http http://127.0.0.1:8000/set module==hvrpc256new username==username password==password parameter==I0Set value==0 slot==2 channels==1,2
HTTP/1.1 200 OK
content-length: 132
content-type: application/json
date: Mon, 10 May 2021 07:09:27 GMT
server: uvicorn

{
    "data": {
        "channels": [
            1,
            2
        ],
        "parameter": "I0Set",
        "slot": 2,
        "value": 0
    },
    "hvwrapper_code": 0,
    "message": "Command wrapper correctly executed"
}
```

## Toggle correction

To enable or disable correction of `V0Set`:

```bash
$ http http://127.0.0.1:8000/toggle_correction module==hvrpc256new username==username password==password slot==2 channels==0 enable==true
HTTP/1.1 200 OK
content-length: 350
content-type: application/json
date: Mon, 10 May 2021 09:44:16 GMT
server: uvicorn

{
    "data": {
        "2": {
            "0": {
                "V0Set": {
                    "correction": true,
                    "value": 10.0
                }
            },
            "1": {
                "V0Set": {
                    "correction": false,
                    "value": 10.0
                }
            },
            "2": {
                "V0Set": {
                    "correction": false,
                    "value": 10119.0
                }
            },
            "3": {
                "V0Set": {
                    "correction": false,
                    "value": 0.0
                }
            },
            "4": {
                "V0Set": {
                    "correction": false,
                    "value": 0.0
                }
            },
            "5": {
                "V0Set": {
                    "correction": false,
                    "value": 10.0
                }
            }
        }
    },
    "hvwrapper_code": 0,
    "message": "State updated."
}
```

## Start a channel

Start sets channels to a voltage and optionally enable or disable pressure/temperature
correction.

```bash
http -v POST http://127.0.0.1:8000/start module==hvrpc256new username==username password==password slot==2 channels==0 parameter==V0Set value==10 enable==true
POST /start?module=hvrpc256new&username=username&password=password&slot=2&channels=0&parameter=V0Set&value=10&enable=true HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 0
Host: 127.0.0.1:8000
User-Agent: HTTPie/2.4.0



HTTP/1.1 200 OK
content-length: 607
content-type: application/json
date: Mon, 10 May 2021 09:41:50 GMT
server: uvicorn

{
    "channels": [
        0
    ],
    "enable": true,
    "module": "hvrpc256new",
    "parameter": "V0Set",
    "response_set": {
        "data": {
            "channels": [
                0
            ],
            "parameter": "V0Set",
            "slot": 2,
            "value": 10
        },
        "hvwrapper_code": 0,
        "message": "Command wrapper correctly executed"
    },
    "response_toggle": {
        "data": {
            "2": {
                "0": {
                    "V0Set": {
                        "correction": true,
                        "value": 10
                    }
                },
                "1": {
                    "V0Set": {
                        "correction": false,
                        "value": 10.0
                    }
                },
                "2": {
                    "V0Set": {
                        "correction": false,
                        "value": 10119.0
                    }
                },
                "3": {
                    "V0Set": {
                        "correction": false,
                        "value": 0.0
                    }
                },
                "4": {
                    "V0Set": {
                        "correction": false,
                        "value": 0.0
                    }
                },
                "5": {
                    "V0Set": {
                        "correction": false,
                        "value": 10.0
                    }
                }
            }
        },
        "hvwrapper_code": 0,
        "message": "State updated."
    },
    "slot": 2,
    "value": 10
}
```

The start endpoint is also supporting grafana alerts, meaning that it will
apply a command if the POST requests contains json body with a key `state`
equals to `alerting`. Other requests with different states are discarded

```bash
http -v POST http://127.0.0.1:8000/start state=alerting module==hvrpc256new username==username password==password slot==2 channels==0 parameter==V0Set value==10 enable==true
```

## Kill a channel

Kill switches off channels, set `V0Set` to 0 and disable p/T correction if enabled.
This command supports both `GET` and `POST` verbs and
provides support to grafana similarly to the `/start` endpoint.

```bash
http -v POST http://127.0.0.1:8000/kill state=alerting module==hvrpc256 username==username password==password slot==0 channels==0
POST /kill?module=hvrpc256&username=username&password=password&slot=0&channels=0 HTTP/1.1
Accept: application/json, */*;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 21
Content-Type: application/json
Host: 127.0.0.1:8000
User-Agent: HTTPie/2.4.0

{
    "state": "alerting"
}


HTTP/1.1 200 OK
content-length: 77
content-type: application/json
date: Wed, 12 May 2021 14:13:01 GMT
server: uvicorn

{
    "data": {},
    "hvwrapper_code": 0,
    "message": "Command wrapper correctly executed"
}
```

In case an error is occuring:

```bash
http -v POST http://127.0.0.1:8000/kill state=alerting module==hvrpc256 username==username password==password slot==2 channels==0
POST /kill?module=hvrpc256&username=username&password=password&slot=2&channels=0 HTTP/1.1
Accept: application/json, */*;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 21
Content-Type: application/json
Host: 127.0.0.1:8000
User-Agent: HTTPie/2.4.0

{
    "state": "alerting"
}


HTTP/1.1 200 OK
content-length: 95
content-type: application/json
date: Wed, 12 May 2021 14:11:33 GMT
server: uvicorn

{
    "data": {},
    "hvwrapper_code": -1,
    "message": "Error in KILL command: 'Slot 2 is not in crate map'"
}
```

# Testing

Since the app is tightly bounded to a physical device you could test the api
it with a real module connected.
