from fastapi import Response, status
from typing import Dict, Union
import zmq
import logging
import models
logger = logging.getLogger(__name__)


def format_module_name(module: str):
    """Return the module name with .cern.ch appended if not present"""
    module = module + '.cern.ch' if '.cern.ch' not in module else module
    return module


def get_socket(module: str, tasks: Dict):
    """Get the zmq socket associated with a module"""
    module_config = tasks.get(module)
    if module_config:
        socket = module_config.get('socket')
        return socket


def config_error_message(module: str, response: Response):
    """Return 400 error if module is not in config"""
    response.status_code = status.HTTP_400_BAD_REQUEST
    return models.Response(hvwrapper_code=None, message=f"Module {module} not in config", data=None), response


def format_channels(channels: str):
    """Format channels as a list, e.g. from '0,1,2' -> [0,1,2]"""
    if isinstance(channels, str):
        channels = [int(channel) for channel in channels.split(',')]
    else:
        channels = [channels]

    return channels


async def send_command(command: str, response: Response, tasks: Dict, module: str, **kwargs):
    """Send a command to the background task via zmq. Return 400 error if the HVWrapper
    return code is not 0. Return  500 error if an exception is raised when communicating
    with the zmq socket."""
    module = format_module_name(module)
    socket = get_socket(module, tasks)
    logger.debug(f"MODULE: {module} SOCKET: {socket}")
    if not socket:
        return config_error_message(module, response)
    try:
        await socket.send_json({"command": command, "args": {"module": module, **kwargs}})
        reply = await socket.recv_json()
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return {"message": e}
    # In case of succesful response but bad hvwrapper error code return
    # a 400 code status with the reply
    if 'hvwrapper_code' in reply and reply["hvwrapper_code"] != 0:
        response.status_code = status.HTTP_400_BAD_REQUEST

    return reply, response


def handle_channels(channels: str, response: Response):
    """Format channels and eventually send a 400 response if their formatted
    in the wrong way"""
    try:
        channels = format_channels(channels)
    except Exception as e:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {"message": f"Channels are {channels} but should be a comma separated list of integers."}
    return channels, response


async def broker(xpub_port: int, xsub_port: int):
    """Proxy messages from XPUB to XSUB socket"""
    ctx = zmq.asyncio.Context()
    xsub = ctx.socket(zmq.XSUB)
    xsub_port = xsub.bind(f"tcp://*:{xsub_port}")
    xpub = ctx.socket(zmq.XPUB)
    xpub_port = xpub.bind(f"tcp://*:{xpub_port}")
    poller = zmq.asyncio.Poller()
    poller.register(xsub, zmq.POLLIN)
    poller.register(xpub, zmq.POLLIN)
    while True:
        events = dict(await poller.poll(10))
        if xsub in events:
            msg = await xsub.recv_multipart()
            await xpub.send_multipart(msg)
        if xpub in events:
            msg = await xpub.recv_multipart()
            await xsub.send_multipart(msg)


async def kill_handler(response: Response,
                       module: str,
                       username: str,
                       password: str,
                       slot: int,
                       channels: Union[str, int],
                       tasks: Dict):
    try:
        channels = format_channels(channels)
    except Exception as e:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {"message": f"Channels are {channels} but should be a comma separated list of integers."}

    # Get the socket and try to send the command
    module = format_module_name(module)
    socket = get_socket(module, tasks)
    if not socket:
        return config_error_message(module, response)
    try:
        msg = {"command": "KILL", "args": {
            "module": module,
            "username": username,
            "password": password,
            "slot": slot,
            "channels": channels,
        }}
        await socket.send_json(msg)
        logger.debug(f"Sent zmq json message: {msg}")
        reply = await socket.recv_json()
        logger.debug(f"Got zmq json reply: {reply}")
    except Exception as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        logger.error(f"Error in getting parameter from API: {e}")
        return {"message": e}

    return reply
